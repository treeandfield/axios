
var aGet = function(){
	console.log('get');
	
	axios.get('http://www.rickytruth.com/example.json')
	.then(function (response) {
	    // handle success
	    var r = response.data;
		
		$('.result p').html('').html('<pre>'+JSON.stringify(r, null, 4)+'</pre>');
	
	})
	.catch(function (error) {
	    // handle error
	    alert(error);
	})
	.then(function () {
	   console.log('get call complete');
	});
  
}

var aPost = function(){
	console.log('post');
	axios.post('api.php', {
	    fname: 'Fred',
	    lname: 'Wreck'
	})
	.then(function (response) {
	    console.log(response);
	    $('.result p').html('').html(response.data);
	})
	.catch(function (error) {
	    console.log(error);
	});
}

$(document).ready(function(){
	console.log('ready');
	$(".get").bind('click', aGet);
	$(".post").bind('click', aPost);
});